from django.urls import path

from . import views

urlpatterns = [
    path("customers", views.list_customers),
    path("customers/create", views.modal_customer_create),
    path("customer/<int:mdat_id>", views.edit_customer),
]
