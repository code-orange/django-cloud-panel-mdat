from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.utils.translation import gettext as _

from django_cloud_panel_main.django_cloud_panel_main.views import get_nav
from django_mdat_customer.django_mdat_customer.models import MdatCustomers
from django_session_ldap_attributes.django_session_ldap_attributes.decorators import (
    admin_group_required,
)
from django_session_ldap_attributes.django_session_ldap_attributes.snippets import (
    get_mdat_customer_for_user,
)


@admin_group_required("VAR")
def list_customers(request):
    template = loader.get_template("django_cloud_panel_mdat/list_customers.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Value Added Reseller")
    template_opts["content_title_sub"] = _("List Customers")

    template_opts["menu_groups"] = get_nav(request)

    mdat = get_mdat_customer_for_user(request.user)
    mdat_customers = MdatCustomers(mdat.id).get_reseller_customers()
    template_opts["all_customers"] = list(mdat_customers.values())

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("VAR")
def edit_customer(request, mdat_id):
    template = loader.get_template("django_cloud_panel_mdat/edit_customer.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Value Added Reseller")
    template_opts["content_title_sub"] = _("Edit Customer")

    template_opts["customer"] = mdat_id

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("VAR")
def modal_customer_create(request):
    template = loader.get_template("django_cloud_panel_mdat/modal_customer_create.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Value Added Reseller")
    template_opts["content_title_sub"] = _("Create Customer")

    return HttpResponse(template.render(template_opts, request))
